/**
 * Writen by Emmanuel Lucio Urbina
 * 2019
 */
const store = require('../../store/db');
const sequelize = store.sequelize;

class EntradasModel extends store.Model {}

EntradasModel.init({
    id_entrada: {
        type: store.Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    nota: {
        type: store.Sequelize.STRING,
    },
    create: {
        type: store.Sequelize.DATEONLY,
    }, // solo fecha
    status: {
        type: store.Sequelize.BOOLEAN,
        defaultValue: 1
    },
    created_at:{
        type: store.Sequelize.DATE,
    },
    updated_at:{
        type: store.Sequelize.DATE,
    }
},{
    timestamps: false,
    sequelize,
    modelName: 'tbl_entradas' // nombre de tabla
});

module.exports = EntradasModel;