/**
 * Write by Emmanuel Lucio Urbina
 * 2019
 * ProductoController
 */
const htmlspecialchars = require('htmlspecialchars');
const Moment = require('moment-timezone');
const helper = require('../Helper/expressions');
const autoload = require('../autoload'); /**LOAD ALL MODELS */
exports.index = (req, res) => {


    let marca = autoload.Marca.findAll({ where: { status: 1 } })
        .then(res => {
            return res;
        })
        .catch(err => {
            return err;
        });
    let categoria = autoload.Categoria.findAll({ where: { status: 1 } })
        .then(res => {
            return res;
        })
        .catch(err => {
            return err;
        });
    /**
     * muestra vista
     */

    Promise.all([categoria, marca])
        .then(promises => {
            res.render('producto/registra', {
                name_app: "Inventario",

                categoria: promises[0],
                marca: promises[1]

            });
        })

}

exports.listByCode = (req, res) => {

    autoload.Producto.findAll({

        where: { codigo: req.params.codigo, status: 1 },
        include: [{ model: autoload.Marca }, { model: autoload.Categoria }],

    })
        .then(response => {
            console.log(response)
            res.render('producto/producto', { name_app: "Inventario", producto: response, page: null, });
        })
        .catch(err => {
            res.send(err);
        })

}


exports.listAll = (req, res) => {
    let limit = 2;   // number of records per page
    let p = (req.params.page - 1) * limit || 0;
    autoload.Producto.findAll({
        where: { status: 1 },
    })
        .then(response => {
            let pages = Math.ceil(response.length / limit);
            autoload.Producto.findAll({
                where: { status: 1, id_prod: { [autoload.Op.gt]: p } },
                include: [{ model: autoload.Marca }, { model: autoload.Categoria }],
                limit: limit
            })
                .then(prods => {
                    res.render('producto/producto', { name_app: "Inventario", producto: prods, page: pages });
                })
                .catch(err => {
                    res.send(err);
                })

        })
        .catch(err => {
            res.send(err)
        })
}

exports.create = (req, res) => {
    let data = {
        tblMarcaIdMarca: htmlspecialchars(req.fields.marca),
        tblCategoriaIdCategoria: htmlspecialchars(req.fields.categoria),
        producto: htmlspecialchars(req.fields.producto_nombre),
        codigo: htmlspecialchars(req.fields.producto_codigo),
        create: Moment.tz('America/Matamoros').format(),
        created_at: Moment.tz('America/Matamoros').format(),
        updated_at: Moment.tz('America/Matamoros').format(),
    }
    if (helper.notEmpty(data.producto) || helper.notEmpty(data.codigo)) {
        /**
         * Valida que no exista registro
         */
        autoload.Producto.findOne({ where: { codigo: data.codigo } })
            .then(resp => {
                if (resp == null) {
                    autoload.Producto.create(data)
                        .then(success => {
                            res.send({ message: "Se guardo correctamente", status: "success" });
                        })
                        .catch(fatal => {
                            res.send({ message: "Ocurrio un error " + fatal, status: "error" });
                        })

                } else {
                    res.send({ message: "Producto ya existe", status: "error" });
                }
            })
            .catch(err => {
                res.send({ message: "Ocurrio un error " + err, status: "error" });
            })
    } else {
        res.send({ message: "Todos los campos son requeridos", status: "error" });
    }
}

/**
 * List Prod. with POST
 */
exports.listCode = (req, res) => {

    autoload.Producto.findAll({

        where: { codigo: req.fields.codigo, status: 1 },
        include: [{ model: autoload.Marca }, { model: autoload.Categoria }],

    })
        .then(response => {
            res.send(response);
        })
        .catch(err => {
            res.send(err);
        })

}