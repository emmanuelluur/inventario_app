/**
 * Writen by Emmanuel Lucio Urbina
 * 2019
 */
const store = require("../../store/db")
const sequelize = store.sequelize;

class ProveedorModel extends store.Model { }
ProveedorModel.init({
    id_prov: {
        type: store.Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    nombre: {
        type: store.Sequelize.STRING,
        validate: { notEmpty: { msg: "Nombre requerido" } }
    },
    contacto: {
        type: store.Sequelize.STRING,
        validate: { notEmpty: { msg: "Contacto requerido" } }
    },
    alias: {
        type: store.Sequelize.STRING,
        validate: { notEmpty: { msg: "Alias requerido" } }
    },
    prov_add: {
        type: store.Sequelize.DATEONLY,
    }, // solo fecha
    status: {
        type: store.Sequelize.BOOLEAN,
        defaultValue: 1
    },
    created_at:{
        type: store.Sequelize.DATE,
    },
    updated_at:{
        type: store.Sequelize.DATE,
    }


},
    {
        timestamps: false,
        sequelize,
        modelName: 'tbl_proveedores' // nombre de tabla
    });


module.exports = ProveedorModel;
