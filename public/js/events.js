const content = document.getElementById("my_app") || null;
const content_buttons = document.getElementById("content-buttons") || null;
const content_modal = document.getElementById("app_modal") || null;
/**
 * eventos principales 
 */
content ?  // valida que exista el contenedor
    content.addEventListener("click", el => {
        if (el.target.getAttribute("id") == "guarda_prod") {
            let form = document.getElementById("form_prod");
            let data = new FormData(form);
            /**
             * call post method para producto
             */
        }
        if (el.target.getAttribute("id") == "guarda_prov") {
            let form = document.getElementById("form_prov");
            let data = new FormData(form);
            /**
             * call post method para proveedor
             */
            post("/proveedor/nuevo",data)
            .then(res =>{
                let resp = JSON.parse(res);
                Swal.fire({
                    position: 'top-end',
                    type: resp.status,
                    title: resp.message,
                    showConfirmButton: false,
                    timer: 1500
                });
                form.reset();
               
            })
            .catch(err => {

                Swal.fire({
                    position: 'top-end',
                    type: 'error',
                    title: 'Ocurrio erro de servidor ' + err ,
                    showConfirmButton: false,
                    timer: 1500
                })
                form.reset();
            })
            

        }
    }) : "";


/**
 * eventos para cargar formularios a modal
 */
content_buttons ? // valida que exista el contenedor
    content_buttons.addEventListener("click", el => {
        if (el.target.getAttribute("id") == "add_categoria") {
            document.getElementById("tituloModal").innerHTML = "Registra Categoría";
            document.getElementById("app_modal").innerHTML = form_cat;
        }
        if (el.target.getAttribute("id") == "add_marca") {
            document.getElementById("tituloModal").innerHTML = "Registra Marca";
            document.getElementById("app_modal").innerHTML = form_marca;

        }
    })
    : "";

/**
 * eventos para acciones de los elementos del modal
 */
content_modal ?  // valida que exista el contenedor
    content_modal.addEventListener("click", el => {
        if (el.target.getAttribute("id") == "save_categoria") {
            let form = document.getElementById("registra_cat");
            let data = new FormData(form);
            /**
             * post method
             */
            post("/categoria/nuevo", data)
            .then(success => {
                let resp = JSON.parse(success);
                Swal.fire({
                    position: 'top-end',
                    type: resp.status,
                    title: resp.message,
                    showConfirmButton: false,
                    timer: 1500
                });
                form.reset();
            })
            .catch(err => {
                Swal.fire({
                    position: 'top-end',
                    type: 'error',
                    title:  'Ocurrio erro de servidor ' + err,
                    showConfirmButton: false,
                    timer: 1500
                });
                form.reset();

            });
        }
        if (el.target.getAttribute("id") == "save_marca") {
            let form = document.getElementById("registra_marca");
            let data = new FormData(form);
            /**
            * call post method
            */
           post("/marca/nuevo", data)
            .then(success => {
                let resp = JSON.parse(success);
                Swal.fire({
                    position: 'top-end',
                    type: resp.status,
                    title: resp.message,
                    showConfirmButton: false,
                    timer: 1500
                });
                form.reset();
            })
            .catch(err => {
                Swal.fire({
                    position: 'top-end',
                    type: 'error',
                    title:  'Ocurrio erro de servidor ' + err,
                    showConfirmButton: false,
                    timer: 1500
                });
                form.reset();

            });
        }
    }) : "";

/**
 * forms modals
 */
const form_cat = `
<form id = 'registra_cat'>
    <div class = 'form-group'>
        <input type = 'text' name = "categoria" class = 'form-control' placeholder = 'Categoría'>
    </div>
    <button type = 'button' class = 'btn btn-success' id = 'save_categoria'>Guardar</button>
</form>`;

const form_marca = `
<form id = 'registra_marca'>
    <div class = 'form-group'>
        <input type = 'text' name = "marca" class = 'form-control' placeholder = 'Marca'>
    </div>
    <button type = 'button' class = 'btn btn-success' id = 'save_marca'>Guardar</button>
</form>`;