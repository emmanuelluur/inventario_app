const app = new Vue({
    el: "#my_app",
    data: {
        producto_nombre: "",
        producto_codigo: "",
    },
    methods: {
        SaveProducto: function () {
            let form = document.getElementById("form_prod");
            let data = new FormData(form);
            post("/producto/nuevo", data)
                .then(success => {
                    let resp = JSON.parse(success);
                    Swal.fire({
                        position: 'top-end',
                        type: resp.status,
                        title: resp.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                    form.reset();
                })
                .catch(err => {
                    Swal.fire({
                        position: 'top-end',
                        type: 'error',
                        title: 'Ocurrio erro de servidor ' + err,
                        showConfirmButton: false,
                        timer: 1500
                    });
                    form.reset();

                });
        }
    }
});

const search = new Vue({
    el: "#search_prod",
    data: {
        codigoSearch: "",
    },
    methods: {
        searchCode: function () {
            window.location.href = this.codigoSearch;
        },
        getAll: function () {
            window.location.href = "/listar/1";
        }
    }
});