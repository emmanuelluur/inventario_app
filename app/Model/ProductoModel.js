/**
 * Writen by Emmanuel Lucio Urbina
 * 2019
 */
const store = require("../../store/db")
const sequelize = store.sequelize;

class ProductoModel extends store.Model { }

ProductoModel.init({
    id_prod: {
        type: store.Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },

    producto: {
        type: store.Sequelize.STRING,
        validate: { notEmpty: { msg: "producto requerido" } }
    },
    codigo: {
        type: store.Sequelize.STRING,
        validate: { notEmpty: { msg: "codigo requerido" } }
    },

    create: {
        type: store.Sequelize.DATEONLY,
    }, // solo fecha
    status: {
        type: store.Sequelize.BOOLEAN,
        defaultValue: 1
    },
    created_at: {
        type: store.Sequelize.DATE,
    },
    updated_at: {
        type: store.Sequelize.DATE,
    }
}, {
        timestamps: false,
        sequelize,
        modelName: 'tbl_productos' // nombre de tabla
    })


module.exports = ProductoModel;