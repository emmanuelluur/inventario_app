/**
 * Write by Emmanuel Lucio Urbina
 * 2019
 */
require('dotenv').config();
const express = require("express");
const path = require("path");
const router = require('./router/router');
const app = express();
const formidableMiddleware = require('express-formidable');



/**
 * test db
 */
// uso de formData
app.use(formidableMiddleware({
    encoding: 'utf-8',
    uploadDir: './public', // file to upload
    multiples: true, // req.files to be arrays of files

}));
/**
 * set views
 */
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
/** 
 * uso de carpeta public para archivos locales
*/
app.use('/public', express.static(__dirname + '/public'));
app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js'));
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist/'));
app.use('/vue', express.static(__dirname + '/node_modules/vue/dist/'));
app.use('/alert', express.static(__dirname + '/node_modules/sweetalert2/dist/'));

/**
 * Rutas
 */
app.use('/',router);

// error
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next();
});
// error handler
// define as the last app.use callback
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.send(err.message);
});

module.exports = app;