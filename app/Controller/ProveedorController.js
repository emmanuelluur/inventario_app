/**
 * Write by Emmanuel Lucio Urbina
 * 2019
 * ProveedorController
 */
const htmlspecialchars = require('htmlspecialchars');
const Moment = require('moment-timezone');
const autoload = require('../autoload'); 
const helper = require('../Helper/expressions'); 

exports.index = (req, res) => {
    res.render('proveedor/registra', { name_app: "Inventario" });
}

exports.create = (req, res) => {
    /**
     * Arreglo de los campos recibidos cliente como FormData
     */
    let data = {
        nombre: htmlspecialchars(req.fields.prov_nombre),
        contacto: htmlspecialchars(req.fields.prov_contacto),
        alias: htmlspecialchars(req.fields.prov_alias),
        prov_add: Moment.tz('America/Matamoros').format(),
        created_at: Moment.tz('America/Matamoros').format(),
        updated_at: Moment.tz('America/Matamoros').format(),
    }
    /**
     * Valida Campos 
     */
    if (helper.notEmpty(data.nombre) && helper.notEmpty(data.contacto) && helper.notEmpty(data.alias)) {
        /**
        * Valida que exista proveedor
        * Procede a guardar si no existe
        */
       autoload.Proveedor.findOne({ where: { alias: data.alias } })
            .then(resp => {
                if (resp == null) {
                    autoload.Proveedor.create(data)
                    .then(success => {
                        res.send({ message: "Se guardo correctamente", status: "success" });
                    })
                    .catch(fatal => {
                        res.send({ message: "Ocurrio un error " + fatal, status: "error" });
                    })
                    
                } else {
                    res.send({ message: "Proveedor ya existe", status: "error" });
                }
            })
            .catch(err => {
                res.send({ message: "Ocurrio un error " + err, status: "error" });
            })
    } else {
        res.send({ message: "Todos los campos son requeridos", status: "error" })
    }


}