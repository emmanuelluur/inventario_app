/**
 * Write by Emmanuel Lucio Urbina
 * 2019
 * EventosEntradaController
 */
const htmlspecialchars = require('htmlspecialchars');
const Moment = require('moment-timezone');
const autoload = require('../autoload'); /** LOAD ALL MODELS */
const helper = require('../Helper/expressions');


exports.index = ((req, res) => {
    res.render("entradas/evento", {name_app: "Inventario", entrada: req.params.codigo, listado: []});
});


exports.create = (req, res) => {
    let data = {
        cantidad: htmlspecialchars(req.fields.cantidad),
        tblEntradaIdEntrada: htmlspecialchars(req.fields.evento),
        tblProductoIdProd: htmlspecialchars(req.fields.producto),
        create: Moment.tz('America/Matamoros').format(),
        created_at: Moment.tz('America/Matamoros').format(),
        updated_at: Moment.tz('America/Matamoros').format(),
    }

    let entrada = autoload.EventoEntrada.build(data);
    entrada.save()
    .then(response => {
        res.send({ message: "Se guardo correctamente", status: "success" });
    })
    .catch(error => {
        res.send({ message: "Ocurrio un error " + fatal, status: "error" });
    });
}