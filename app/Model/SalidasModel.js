/**
 * Writen by Emmanuel Lucio Urbina
 * 2019
 */
const store = require('../../store/db');
const sequelize = store.sequelize;

class SalidasModel extends store.Model {}

SalidasModel.init({
    id_salida: {
        type: store.Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    nota: {
        type: store.Sequelize.STRING,
    },
    status: {
        type: store.Sequelize.BOOLEAN,
        defaultValue: 1
    },
    created_at:{
        type: store.Sequelize.DATE,
    },
    updated_at:{
        type: store.Sequelize.DATE,
    }
},{
    timestamps: false,
    sequelize,
    modelName: 'tbl_salidas' // nombre de tabla
});

module.exports = SalidasModel;