/**
 * Funciones de ayuda
 * Write by Emmanuel Lucio Urbina
 * 2019
 */
exports.notEmpty = field => {
    /**
     * con expresion regular se valida que el campo no este vacio
     * incluyendo espacios en blanco solamente
     */
    let result = true;
    if (/^\s*$/.test(field)) {
        result = false;
    }
    return result;
}