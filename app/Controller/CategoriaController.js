/**
 * Write by Emmanuel Lucio Urbina
 * 2019
 * CategoriaController
 */
const htmlspecialchars = require('htmlspecialchars');
const Moment = require('moment-timezone');
const autoload = require('../autoload'); /** LOAD ALL MODELS */
const helper = require('../Helper/expressions');

exports.create = (req, res) => {
    /**
     * Arreglo de campos recibidos por cliente
     */
    let data = {
        categoria: htmlspecialchars(req.fields.categoria),
        create: Moment.tz('America/Matamoros').format(),
        created_at: Moment.tz('America/Matamoros').format(),
        updated_at: Moment.tz('America/Matamoros').format(),
    }
    /**
     * Valida campos
     */
    if (helper.notEmpty(data.categoria)) {
        autoload.Categoria.findOne({ where: { Categoria: data.categoria } })
        .then(resp => {
            if (resp == null) {
                autoload.Categoria.create(data)
                .then(success => {
                    res.send({ message: "Se guardo correctamente", status: "success" });
                })
                .catch(fatal => {
                    res.send({ message: "Ocurrio un error " + fatal, status: "error" });
                })
                
            } else {
                res.send({ message: "Categoria ya existe", status: "error" });
            }
        })
        .catch(err => {
            res.send({ message: "Ocurrio un error " + err, status: "error" });
        })
    } else {
        res.send({ message: "Todos los campos son requeridos", status: "error" });
    }
}