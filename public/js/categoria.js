const Categoria = new Vue({
    el: "#appCategoria",
    data: {
        categoria: "",
    },
    methods: {
        SaveCategoria: function () {
            let data = new FormData();
            data.append("categoria", this.categoria);
            post("/categoria/nuevo", data)
                .then(success => {
                    let resp = JSON.parse(success);
                    Swal.fire({
                        position: 'top-end',
                        type: resp.status,
                        title: resp.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                    setTimeout(() => {
                        location.href = "/producto/nuevo"
                    }, 1500);
                })
                .catch(err => {
                    Swal.fire({
                        position: 'top-end',
                        type: 'error',
                        title: 'Ocurrio erro de servidor ' + err,
                        showConfirmButton: false,
                        timer: 1500
                    });
                    setTimeout(() => {
                        location.href = "/producto/nuevo"
                    }, 1500);

                });
        }
    }
});