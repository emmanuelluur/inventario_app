/**
 * Write by Emmanuel Lucio Urbina
 * 2019
 * IndexController
 */

exports.getIndex = (req, res) => {
    //  retorna vista inicial, mas el titulo de la barra de navegación
    res.render("index", {name_app:"Inventario"})
}