/**
 * Write by Emmanuel Lucio Urbina
 * 2019
 */
const express = require("express");
const router = express.Router();
/** uso de controladores */
const IndexController = require('../app/Controller/IndexController');
const ProductoController = require('../app/Controller/ProductoController');
const ProveedorController = require('../app/Controller/ProveedorController');
const MarcaController = require('../app/Controller/MarcaController');
const CategoriaController = require('../app/Controller/CategoriaController');
const EntradasController = require('../app/Controller/EntradasController');
const SalidasController = require('../app/Controller/SalidasController');

const EventosEntradaController = require('../app/Controller/EventosEntradaController');
/**
 * Rutas GET
 */
router.get("/", IndexController.getIndex);

router.get("/proveedor/nuevo", ProveedorController.index);
router.get("/producto/nuevo", ProductoController.index);
router.get("/producto/:codigo", ProductoController.listByCode);
/**Paginado productos */
router.get("/listar/:page", ProductoController.listAll);
router.get("/listar/", ProductoController.listAll);

/**
 * entradass
 */
router.get('/entradas/', EntradasController.listAll);
router.get('/salidas/', SalidasController.listAll);

router.get('/evento/entrada/:codigo', EventosEntradaController.index);
/**
 * RUTAS POST
 */
router.post("/proveedor/nuevo", ProveedorController.create);
router.post("/marca/nuevo", MarcaController.create);
router.post("/categoria/nuevo", CategoriaController.create);
router.post("/producto/nuevo", ProductoController.create);

router.post("/producto/listar", ProductoController.listCode);
router.post("/entradas/nuevo", EntradasController.create);
router.post("/salidas/nuevo", SalidasController.create);

router.post("/evento/entrada/nuevo", EventosEntradaController.create);
/** Manejo personalizado de rutas no encontradas */
router.get("*", (req, res) => {
    res.send("Recurso no encontrado");
});

module.exports = router;