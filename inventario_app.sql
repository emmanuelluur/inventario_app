-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-04-2019 a las 04:05:12
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `inventario_app`
--
CREATE DATABASE IF NOT EXISTS `inventario_app` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci;
USE `inventario_app`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_categoriaa`
--

CREATE TABLE IF NOT EXISTS  `tbl_categoria` (
  `id_cat` bigint(20) NOT NULL,
  `categoria` VARCHAR(60) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `fecha_create` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_entradas`
--

CREATE TABLE IF NOT EXISTS `tbl_entrada` (
  `id_ent` bigint(20) NOT NULL,
  `id_prod` bigint(20) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `fecha_entrada` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_marca`
--

CREATE TABLE IF NOT EXISTS `tbl_marca` (
  `id_marca` bigint(20) NOT NULL,
  `marca` VARCHAR(60) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `fecha_create` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_productos`
--

CREATE TABLE IF NOT EXISTS `tbl_producto` (
  `id_prod` bigint(20) NOT NULL,
  `id_prov` bigint(20) NOT NULL,
  `id_marca` bigint(20) NOT NULL,
  `id_cat` bigint(20) NOT NULL,
  `nombre` varchar(60) DEFAULT NULL,
  `ident` VARCHAR(60) NOT NULL,
  `prod_add` date DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_proveedor`
--

CREATE TABLE IF NOT EXISTS `tbl_proveedor` (
  `id_prov` bigint(20) NOT NULL,
  `nombre` varchar(60) DEFAULT NULL,
  `contacto` varchar(60) DEFAULT NULL,
  `alias` VARCHAR(60) NOT NULL,
  `prov_add` date DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_salidas`
--

CREATE TABLE IF NOT EXISTS `tbl_salida` (
  `id_sal` bigint(20) NOT NULL,
  `id_prod` bigint(20) NOT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `fecha_salida` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_categoriaa`
--
ALTER TABLE `tbl_categoria`
  ADD PRIMARY KEY (`id_cat`);

--
-- Indices de la tabla `tbl_entradas`
--
ALTER TABLE `tbl_entrada`
  ADD PRIMARY KEY (`id_ent`);

--
-- Indices de la tabla `tbl_marca`
--
ALTER TABLE `tbl_marca`
  ADD PRIMARY KEY (`id_marca`);

--
-- Indices de la tabla `tbl_productos`
--
ALTER TABLE `tbl_producto`
  ADD PRIMARY KEY (`id_prod`);

--
-- Indices de la tabla `tbl_proveedor`
--
ALTER TABLE `tbl_proveedor`
  ADD PRIMARY KEY (`id_prov`);

--
-- Indices de la tabla `tbl_salidas`
--
ALTER TABLE `tbl_salida`
  ADD PRIMARY KEY (`id_sal`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbl_categoriaa`
--
ALTER TABLE `tbl_categoria`
  MODIFY `id_cat` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_entradas`
--
ALTER TABLE `tbl_entrada`
  MODIFY `id_ent` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_marca`
--
ALTER TABLE `tbl_marca`
  MODIFY `id_marca` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_productos`
--
ALTER TABLE `tbl_producto`
  MODIFY `id_prod` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_proveedor`
--
ALTER TABLE `tbl_proveedor`
  MODIFY `id_prov` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_salidas`
--
ALTER TABLE `tbl_salida`
  MODIFY `id_sal` bigint(20) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
