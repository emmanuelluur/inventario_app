/**
 * Sincroniza modelos
 * Crea relaciones entre modelos
 * Writed by Emmanuel Lucio Urbina
 */
const store = require("../store/db");
const Proveedor = require("./Model/ProveedorModel");
const Marca = require("./Model/MarcaModel");
const Categoria = require("./Model/CategoriaModel");
const Entradas = require("./Model/EntradasModel");
const Salidas = require("./Model/SalidasModel");
const EventoEntrada = require("./Model/EventoEntradaModel");
const EventoSalida = require("./Model/EventoSalidaModel");
const Producto = require("./Model/ProductoModel");

const Op = store.Operators;
const sequelize = store.sequelize;
/**
 * Relaciones 1 - M
 */

Marca.hasMany(Producto);
Categoria.hasMany(Producto);
Producto.belongsTo(Marca);
Producto.belongsTo(Categoria);
//  Entradas/ Salidas
Proveedor.hasMany(Entradas);
Entradas.belongsTo(Proveedor);

/** 
 * Relaciones N - M
 * Eventos de salidas/entradas
 * Lista de productos
 */
Producto.belongsToMany(Entradas, {through: EventoEntrada});
Entradas.belongsToMany(Producto, {through: EventoEntrada});

Producto.belongsToMany(Salidas, {through: EventoSalida});
Salidas.belongsToMany(Producto, {through: EventoSalida});

/**
 * Sincroniza/Genera las tablas con las relaciones
 */


sequelize.sync()
.then(() => {
    console.log("Modelos Cargados")
} )
.catch(err => {
    console.log(err);
})

module.exports = {
    Entradas,
    Salidas,
    EventoEntrada,
    EventoSalida,
    Producto,
    Proveedor,
    Marca,
    Categoria,
    Op // permite en el where el > >= || en WHERE
}