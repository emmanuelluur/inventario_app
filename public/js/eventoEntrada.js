function appEventoEntrada() {
    new Vue({
        el: "#contentProd",
        data: {
            cantidadEntrada: 0,
        },
        methods: {
            AgregaEntrada: function () {
                let idEntrada = document.getElementById("idEntrada").value;
                let idProd = document.getElementById("productoId").value;
                let data = new FormData();
                data.append("evento", idEntrada);
                data.append("cantidad", this.cantidadEntrada);
                data.append("producto", idProd);

                post("/evento/entrada/nuevo", data)
                    .then(success => {
                        let resp = JSON.parse(success);
                        Swal.fire({
                            position: 'top-end',
                            type: resp.status,
                            title: resp.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                        setTimeout(() => {
                            location.reload();
                        }, 1600);
                    })
                    .catch(err => {
                        Swal.fire({
                            position: 'top-end',
                            type: 'error',
                            title: 'Ocurrio erro de servidor ' + err,
                            showConfirmButton: false,
                            timer: 1500
                        });
                        setTimeout(() => {
                            location.reload();
                        }, 1600);

                    });
            }
        }
    });
}
const appEntrada = new Vue({
    el: "#appProd",
    data: {
        codigo_producto: "",
        productoResponse: "",
        classResp: "",
    },
    methods: {
        BuscaProducto: function () {
            let producto = this.codigo_producto;
            let data = new FormData();
            data.append('codigo', producto);
            post("/producto/listar", data)
                .then(response => {
                    let prod = JSON.parse(response);
                    if (prod.length != 0) {
                        this.productoResponse = prod[0].producto;
                        this.classResp = "alert alert-primary";
                        console.log(prod);
                        let idProd = document.createElement("input");
                        idProd.setAttribute("type", "hidden");
                        idProd.setAttribute("id", "productoId")
                        idProd.setAttribute("value", prod[0].id_prod);
                        let content = document.getElementById("contentProd");
                        let divForm = document.createElement("div");
                        divForm.setAttribute("class", "form-group");
                        let inputCant = document.createElement("input");
                        inputCant.setAttribute("type", "number");
                        inputCant.setAttribute("v-model", "cantidadEntrada");
                        inputCant.setAttribute("class", "form-control");
                        let labelCant = document.createElement("label");
                        let txtCant = document.createTextNode("Cantidad Entrada: ");
                        let buttonAdd = document.createElement("button");
                        buttonAdd.setAttribute("v-on:click", "AgregaEntrada");
                        buttonAdd.setAttribute("class", "btn btn-success");
                        let txtbtn = document.createTextNode("Agregar");
                        buttonAdd.appendChild(txtbtn);
                        labelCant.appendChild(txtCant);
                        divForm.appendChild(labelCant);
                        divForm.appendChild(inputCant);

                        content.appendChild(divForm);
                        content.appendChild(buttonAdd);
                        content.appendChild(idProd);
                        appEventoEntrada();
                    } else {
                        this.productoResponse = "Sin resultados";
                        this.classResp = "alert alert-danger";
                    }



                })
                .catch(error => {
                    console.log(error);
                });
        }
    }
});

