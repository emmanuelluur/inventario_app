/**
 * Writen by Emmanuel Lucio Urbina
 * 2019
 */
const store = require("../../store/db")
const sequelize = store.sequelize;

class MarcaModel extends store.Model { }
MarcaModel.init({
    id_marca: {
        type: store.Sequelize.BIGINT,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    marca: {
        type: store.Sequelize.STRING,
        validate: { notEmpty: { msg: "Nombre requerido" } }
    },
    create: {
        type: store.Sequelize.DATEONLY,
    }, // solo fecha
    status: {
        type: store.Sequelize.BOOLEAN,
        defaultValue: 1
    },
    created_at:{
        type: store.Sequelize.DATE,
    },
    updated_at:{
        type: store.Sequelize.DATE,
    }


},
    {
        timestamps: false,
        sequelize,
        modelName: 'tbl_marcas' // nombre de tabla
    });

module.exports = MarcaModel;

