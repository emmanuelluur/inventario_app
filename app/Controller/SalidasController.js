/**
 * Write by Emmanuel Lucio Urbina
 * 2019
 * EntradasController
 */
const htmlspecialchars = require('htmlspecialchars');
const Moment = require('moment-timezone');
const autoload = require('../autoload'); /** LOAD ALL MODELS */
const helper = require('../Helper/expressions');

exports.index = (req, res) => {
    res.send("Entradas");
}

exports.create = (req, res) => {
    let data = {
        nota: htmlspecialchars(req.fields.nota),
        create: Moment.tz('America/Matamoros').format(),
        created_at: Moment.tz('America/Matamoros').format(),
        updated_at: Moment.tz('America/Matamoros').format(),
    }
    if (helper.notEmpty(data.nota)) {
        autoload.Salidas.findOne({
            where: { 
                nota: data.nota
            }
        })
            .then(resp => {
                if (resp == null) {
                    autoload.Salidas.create(data)
                        .then(success => {

                            res.send({ message: "Se guardo correctamente", status: "success" });
                        })
                        .catch(fatal => {
                            res.send({ message: "Ocurrio un error " + fatal, status: "error" });
                        })

                } else {
                    res.send({ message: "Nota ya registrada", status: "error" });
                }
            })
            .catch(err => {
                res.send({ message: "Ocurrio un error " + err, status: "error" });
            })
    } else {
        res.send({ message: "Todos los campos son requeridos", status: "error" });
    }
}

exports.listAll = (req, res) => {
    autoload.Salidas.findAll({
        where: { status: 1 },
    })
        .then(response => {
            res.render("Salidas/lista", { name_app: "Inventario", salida: response });
        })
        .catch(err => {
            res.send({ message: "Ocurrio un error " + err, status: "error" });
        })
}