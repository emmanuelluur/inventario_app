const app = new Vue({
    el: '#my_app',
    data: {
        prov_nombre: "",
        prov_contacto: "",
        prov_alias: ""
    },
    methods: {
        GuardaProveedor: function () {
            let form = document.getElementById("form_prov");
            let data = new FormData(form);
            post("/proveedor/nuevo", data).then(res => {
                let resp = JSON.parse(res);
                Swal.fire({position: 'top-end', type: resp.status, title: resp.message, showConfirmButton: false, timer: 1500});
                form.reset();

            }).catch(err => {

                Swal.fire({
                    position: 'top-end',
                    type: 'error',
                    title: 'Ocurrio erro de servidor ' + err,
                    showConfirmButton: false,
                    timer: 1500
                })
                form.reset();
            })
        }
    }
});